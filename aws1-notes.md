# Cloud
Le **cloud** ou **cloud computing** désigne le stockage et l'accès aux données via Internet.
## Les avantages du cloud
- **L'approvisionnementen libre-service**: accéder à n'importe quelle ressource informatique à la demande;
- **L'élasticité**: offre l'opportunité d'augmenter ou de réduire la consommation de ressources en fonction des besoins de l'entreprise.
- **Le paiement à l'utilisation**: Ne payer que des ressources consommées.

## Les catégories de cloud
- **Le cloud public**: service fournit par un tiers, par intermédiaire d'Internet. Le service est vendu sur demande généralement dans une minute ou l'heure qui suit la requête. Le client paye ce qu'il consomme. 
Fournisseurs de cloud public: *Amazon Web Service*, *IBM*, *Microsoft Azure*, *Google compute Engine*
- **Le cloud privée**:
infrastructure entièrement dédiée à une entreprise unique, pouvant être gérée en interne ou par un tiers, et hébergée en interne ou externe.
- **Le cloud hybride**: croisement entre le cloud public et privé Son objectif est de créer *un environement unifié, automatisé et scalable*.

## Les types de cloud

- **IaaS**: (*Infrastructure as a Service*): Un pretataire vous fournit un accès à tout ou une partie de son infrastructure technique (serveurs).
- **PaaS**: (*Platform as a Service*): On vous fournit un accès à l'infrastructure et le nombre de machines nécessaire pour que votre application fonctionne quelque soit la charge de trafic. Vous pouvez avoir accès par exemple aux serveurs de BD, au emails, serveurs de caches... exple de PaaS: *AWS*.
- **SaaS**: (*Software as a Service*): Accès à un logiciel sous forme de service. exple: Google Apps, Office 365.

**Note**: Le service AWS est gratuit pendant un an.

SLA (*Service Level Agreement*) pour garantir financièrement le bon fonctionnement du site si vous êtes une entreprise et que le site est critique.

## Les services aws cloud incontournables

1. **EC2: Elastic Compute Cloud**: permet de gérer des serveurs sous forme de machines virtuelles dans le cloud.Le tout premier service lancé par aws. Il inclus l'offre du *Iaas*.
2. **RDS: Relational Database Service**: permet de gérer les bases de données managées dans le cloud.
3. **S3: Simple Storage Service**:  service de stockage et de distribution de fichiers.
4. D'autres services:
- **Lambda**: exécution de code sans serveur
- **Aurora**: base de données SQL compatible MySQL et PostgreSQL
- **DynamoDB**: Base de données NoSQL
- **Route 53**: Service DNS pour les noms de domaines.
- **API Getway**: création d'API
- **IAM**: Gestion des identités
- **SNS**: envoi de notifications