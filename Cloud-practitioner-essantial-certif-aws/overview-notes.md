# Cloud practitioner Essentials training
teacher: *Kirsten Dupart*

By the end of this course, I should be able to:

1. **Define what the AWS cloud is and its basic global infrastructure**

2. **Describe basic AWS Cloud architectural principles**

3. **Understand the AWS cloud value proposition**
4. **Talk about key AWS services and their common use case**

5. **Describe basic security and compliance aspects of the AWS platform and the shared security model**

6. **Define the billing, account, management, and pricing models**

**Note**: billing - *Facturation*

7. **Identify course of documentation or technical assistance**

8. **Describe basic characteristics of deploying and operating in the AWS cloud**  