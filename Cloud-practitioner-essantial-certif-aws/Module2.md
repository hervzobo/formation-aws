# AWS core services

## EC2: Elastic Cloud Compute
### Amazon EC2 instances
- Pay as you go
- Broad selection of Hardware/Software
- Global hosting

*AMI*: **Amazon Machine Image**
Create EC2 instance:
- step1: *Choose AMI*
- step2: *Choose instance type*
- step3: *Configure instance*
- step4: *Add storage*
- step5: *add tags*: consists of a case-sensitive key-value pair

exple: key=name; value= webserver
- step6: *configure security group*: is a set of firewall rules that control the traffic for your instance.

## EBS: Elastic Block Store
### Overview EBS Volume
- Choose between *HDD* or *SSD* types
- Persistent and customizable block storage for EC2 instances
- Replicated in the same availability zone
- Backup using Snapshots
- Easy and transparent Encryption
- Elastic volumes