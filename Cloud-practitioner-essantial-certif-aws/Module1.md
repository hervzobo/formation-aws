
# AWS cloud 
By the end of this module, I should be able to:

*Define what the AWS cloud is and the basic global infrastructure*

## Defining cloud computing

**Cloud computing** : refers to the on-demand delivery of IT resources and applications via Internet.

The AWS cloud approach of management, testing, reliability and capacity planning is more Agile and efficient.

By using the AWS cloud:
- Automatically scale our computing to meet our needs
- Ensure reliable coverage even in the face of a natural disaster
- reducing risks

Cloud computing ca help you reduce risk by being agile-being able to learn and quickly adapt to change.

The cloud computing allows enterprises to response quickly and elastically to changing market conditions. This facilitates scalability, agility, and innovation.

## The cloud scalability
*Scalability*, means the ability to resize your resources as necessary.
By using AWS, customers grow, shrink, and adapt their consumption of services to meet seasonal requirements, launch new services or products, or simply accommodate new strategic directions.

Compagnies are moving to the cloud is increased agility.

Three main factors that influence agility:
- Increasing speed
- The ease of experimentation 
- And cultivating a culture of innovation

## Cloud elasticity

So like agility, *elasticity* is also a powerful force in cloud computing. It is the power to scale computing resources up or down easily.
You can:
- Quickly deploy new applications
- Instantly scale up as the workload grows
- Instantly shut down resources that are no longer required
**Note**: Using AWS tools like *Auto Scaling* and *Elastic Load Balancing*, your application can automatically scale up or down based on demand.

With AWS, You can easely deploy your system in *multiple regions around the world while providing lower latency and a better experience for your customers at minimal cost*.

## Cloud reliability

*Reliability* is the ability of a system to recover from infrastructure or service failure.

In cloud computing, *Reliability* means being able to acquire computing resources to meet demand and mitigate disruptions.

By using AWS, organizations can archieve *greater flexibility* and *capacity,* reducing the uncertainty of forecasting hardware needs.

*Fault tolerance* means a system can remain operational even if some of the components of that system fail.

## Security
top priority for Amazon
- secure data centers use state-of-art electronic surveillance and multi-factor access control systems.
- Multiple geographic regions and availability Zones allow you  to remain *resilient* in fact of most failures, including natural disasters or system failure.

AWS offers a broad set of global cloud-based products, including: *compute*, *storage*, *databases*, *analytics*
, *networking*, *mobile*, *developer tools*, *internet of things (IoT)*, *security* and *enterprise applications*.

These services help organizations move *faster*, *lower IT costs*, and *scale*

# AWS interfaces

By end of this module, I should be able to:

- To choose the convenient options for accessing and using AWS resources.

## Manage AWS resources
AWS users can create and manage resources in three unique ways:
- *AWS management console*: Easy to use graphical interface that supports majority of AWS.
- *Command Line Interface (CLI)*: Access to services via discrete command.
- *Software Development kits (SDKs)*: incoporate the connectivity and fonctionality of the wide range of AWS cloud services into your code.

## Usability benefits

### AWS management console
- Navigation
- Usability
- Convenient mobile app

### Command Line Interface (CLI)
- Programming language agnostic
- Flexibility to create script

### Software Development Kits (SDKs)
- Ability to use AWS in existing applications
- Flexibility to create applications